//benchmark_fmoz.c

#include<stdio.h>
#include<stdlib.h>

#define TOL		(0.00001)
#define MAX_COL_NBR (6)
#define MAX_ROW_NBR (18)


void print_matrix(double** A, double* c, int s, int r) {
    int i;
    int j;

    for (i = 0; i < s; i++) {
        for (j = 0; j < r; j++) {
            printf("%f ", A[i][j]);
        }
        printf(" : %f \n", c[i]);
    }
    printf("\n");
}

void print_result(int a) {
    if (a)
        printf("Has solution\n");
    else
        printf("Has no solutions :C \n");
}

int make_solution(double* q, int s, int n1, int n2){
    int i;

    if ( n1 > 0 && (n2-n1) > 0) {
        double max = -100000;
        double min = 100000;
        for(i = 0; i < n1; i++) 
            if (q[i] < min)
                min = q[i];

        for(i=n1; i < n2; i++) 
            if (q[i] > max)
                max = q[i];

        if (min - max < 0){
            return 0;
        }else{
            return 1;
        }
    }
    else if ((s - n2) > 0) {
        for (i = n2; i < s; i++) 
            if (q[i] < 0) { 
                return 0;
            }
        return 1;
    }
    else {
        return 1;
    }
}


int fourier_motzkin_elimination(double** A, double* c, int r, int s){
	double** T = A;
   
	double* q = c;
    int s2 = 0;
	while (1) {
        
		//print_matrix(T, q, s, r);
		int n1 = 0;
		int n2 = 0;
        int n3 = 0;
		
		double** pos = calloc(s, sizeof(double*));
		double** neg = calloc(s, sizeof(double*));	
		double** zero = calloc(s, sizeof(double*));
		int i;
		for (i = 0; i < s; i++) {
			pos[i] = calloc(r, sizeof(double));
			neg[i] = calloc(r, sizeof(double));
			zero[i] = calloc(r, sizeof(double));
		}
		double* pos_q = calloc(s, sizeof(double));
		double* neg_q = calloc(s, sizeof(double));
		double* zero_q = calloc(s, sizeof(double));


		for (i = 0; i < s; i++) {
			if (T[i][(r-1)] > TOL) {
				pos[n1] = T[i];
				pos_q[n1] = q[i];
				n1++;				
			}	
			else if ( T[i][r-1] < -TOL) {
				neg[n2]	= T[i];
				neg_q[n2] = q[i];
				n2++;
			} 
			else {
				zero[n3] = T[i];
				zero_q[n3] = q[i];
                n3++;
			}
		}
        
        
			
		
		for (i = 0; i < n2; i++) {
			pos[n1 + i] = neg[i];
			pos_q[n1+i] = neg_q[i];
		}
		n2 += n1;
		for (i = 0; i < s-n2; i++) {
			pos[n2 + i] = zero[i];
			pos_q[n2 + i] = zero_q[i];
		}

        


		T = pos;
		q = pos_q;
        //print_matrix(T, q, s, r);

		for (i = 0; i < n2; i++) {
			q[i] = q[i]/T[i][r-1];
		}
		int j;
		for (i = 0; i < n2; i++) {
			for (j = 0; j < r; j++) {
				T[i][j] = T[i][j]/T[i][r-1];
			}
		}
        //print_matrix(T, q, s, r);
		//printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
		if (r == 1) {
			return make_solution(q, s, n1, n2);
		}

		s2 = s - n2 + n1*(n2-n1);
		r--;
		
		
		double** new_T = calloc(s2, sizeof(double*));
		double* new_q = calloc(s2, sizeof(double));
    	for (i = 0; i < s2; i++) {
            //printf("%zu \n", new_T[i]);
			new_T[i] = calloc(r, sizeof(double));
		}
		
        //printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
		int k;
		for (i = 0; i < n1; i++) {
			for (j = n1; j < n2; j++) {
				for (k = 0; k < r; k++) {
					new_T[i*(n2-n1) + j - n1][k] = T[i][k] - T[j][k];
				}
				new_q[i*(n2-n1) + j - n1] = q[i] - q[j];
			}
		}
		
		for (i = 0; i < s-n2; i++) {
			new_T[n1*(n2-n1) + i] = zero[i];
			new_q[n1*(n2-n1) + i] = zero_q[i];
		}
		T = new_T;
		q = new_q;
		s = s2;
		printf("%d \n",s);
		
		//printf("Test 2, %d %d %d\n", s, n1, n2);
	}
}




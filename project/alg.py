from __future__ import division
import numpy as np
import project_parser
import argparse
import time

'''
A1 = [[ 5, -2], [-15, 6]]
c1 = [-8, -2]

A3 = [[1, -2, 3, -4, 5, -6],
[-2, 3, -4, 5, -6, 7],
[3, -4, 5, -6, 7, -8],
[-4, 5, -6, 7, -8, 9],
[5, -6, 7, -8, 9, -10],
[-6, 7, -8, 9, -10, 11]]
c3 = [-1, -2, -3 ,-4, -5, -6]
'''
tol = 0.00000001

def fourier_motzkin_elimination(A, c):
    r = len(A[0])
    s = len(A)
    T = A
    q = c

    b = []
    B = []

    while True:
        n1 = 0
        n2 = 0
        pos = []
        neg = []
        zero = []
        pos_q = []
        neg_q = []
        zero_q = []
        for j in range(s):
            if T[j][r-1] > tol:
                n1 += 1
                pos.append(T[j])
                pos_q.append(q[j])
            elif T[j][r-1] < -tol:
                n2 += 1
                neg.append(T[j])
                neg_q.append(q[j])
            else:
                zero.append(T[j])
                zero_q.append(q[j])
        
        n2 += n1        
        pos.extend(neg)
        pos.extend(zero)
        pos_q.extend(neg_q)
        pos_q.extend(zero_q)
        T = pos
        q = pos_q

        #print T, q
        
        for j in range(n2):
            q[j] /= T[j][r-1]
        for i in range(r):            # To simplify makeSolution
            for j in range(n2):
                T[j][i] /= T[j][r-1]
        
    
        #print T, q

        if (r == 1):
            return makeSolution(q, n1, n2)
            

        
    
        s = s - n2 + n1*(n2-n1)

        new_T = []        
        new_q = []
        for i in range(n1):
            for j in range(n1,n2):
                new_T.append([T[i][k] - T[j][k] for k in range(r-1)])
                new_q.append(q[i]-q[j])
        new_T.extend(zero)
        new_q.extend(zero_q)
        T = new_T
        q = new_q
        r = r-1
        #print s


def makeSolution(q, n1, n2):
    #print len(q), n1, n2
    if n1 > 0 and n2 - n1 > 0:    #both should be positive!
        if (np.min(q[:n1]) - np.max(q[n1:n2])) < 0:
            return 0
        else:
            return 1

    elif len(q)-n2 > 0:
        if not np.all([q[i] > 0 for i in range(n2,len(q))]):
            return 0
        else:
            return 1
    else:
        return 1
    
    




if __name__ == '__main__':
    #parse command line for input example  
    parser = argparse.ArgumentParser()
    parser.add_argument('-n',help="Select input example file (0-5)",dest='file_number',default='0')
    args = parser.parse_args()

    #parse files for matrices
    A,c,facit = project_parser.parse_file(int(args.file_number))
    #print A,c
    seconds = 4
    t = time.clock()
    count = 0
    while(time.clock() - t < seconds):
        fourier_motzkin_elimination(A, c)
        count += 1

    print count
    #print "Should have been: {0:s}".format(facit)















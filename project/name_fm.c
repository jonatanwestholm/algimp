#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#define TOL		(0.000001)
#define MAX_C (6)
#define MAX_R (50)
#define TIMING (0)
#define TEST (0)

static unsigned long long	fm_count;
static volatile bool		proceed = false;


FILE *f;

static void done(int unused)
{
	proceed = false;
	unused = unused;
    
}

	
unsigned long long name_fm(char* aname, char* cname, int seconds)
{
	FILE*		afile = fopen(aname, "r");
	FILE*		cfile = fopen(cname, "r");

    #if TIMING
    struct timeval	tv;
    #endif

	fm_count = 0;

	if (afile == NULL) {
		fprintf(stderr, "could not open file A\n");
		exit(1);
	}

	if (cfile == NULL) {
		fprintf(stderr, "could not open file c\n");
		exit(1);
	}

    int count = 0;
    int s0 = 0;
    int r0 = 0;
	
    fscanf(afile, "%d", &s0);
    fscanf(afile, "%d", &r0);

	int s = s0;
	int r = r0;
    
	int i;
    int j;
 	int k;
	int s2 = 0;
	int n1;
	int n2;
	int n3;
		
	int pos[MAX_R];
	int neg[MAX_R];
	int zero[MAX_R];
    
    int B[MAX_R][MAX_C];
    double *restrict *restrict T = malloc(MAX_R*sizeof(double*));
	double *restrict *restrict A1 = malloc(MAX_R*sizeof(double*));
	//double** pos = malloc(MAX_R*sizeof(double*));
	//double** neg = malloc(MAX_R*sizeof(double*));
	//double** zero = malloc(MAX_R*sizeof(double*));
	double *restrict *restrict new_T = malloc(MAX_R*sizeof(double*));
	for (i = 0; i < MAX_R; i++) {
		T[i] = malloc(MAX_C*sizeof(double));
		A1[i] = malloc(MAX_C*sizeof(double));
		//pos[i] = malloc(MAX_C*sizeof(double));
		//neg[i] = malloc(MAX_C*sizeof(double));
		//zero[i] = malloc(MAX_C*sizeof(double));
		new_T[i] = malloc(MAX_C*sizeof(double));
	}

    for (i = 0; i < s; i++) {
        for (j = 0; j < r; j++) {
            fscanf(afile, "%d", &B[i][j]);
            T[i][j] = B[i][j];
			A1[i][j] = B[i][j];
        }
    }
    
    int d[MAX_R];
    double* c = malloc(MAX_R*sizeof(double));
	double* c1 = malloc(MAX_R*sizeof(double));
	//double* pos_q = malloc(MAX_R*sizeof(double));
	//double* neg_q = malloc(MAX_R*sizeof(double));
	//double* zero_q = malloc(MAX_R*sizeof(double));
	double* new_q = malloc(MAX_R*sizeof(double));

	

	double *restrict *restrict swap = malloc(MAX_R*sizeof(double*));
	for (i = 0; i < MAX_R; i++) 
		swap[i] = malloc(MAX_C*sizeof(double));
	double* qswap = malloc(MAX_R*sizeof(double));
	double** temp;
	double* qtemp = c;
	
	double* q = c;
    int huggabugga;
    fscanf(cfile, "%d", &huggabugga);
    for (i = 0; i < s; i++) {
        fscanf(cfile, "%d", &d[i]);
        c[i] = d[i];
		c1[i] = d[i];
    }
	/*

	for (i = 0; i < s; i++) {
        for (j = 0; j < r; j++) {
            printf("%f, ", A[i][j]);
        }
        printf(" : %f \n", c[i]);
    }
	*/
	fclose(afile);
	fclose(cfile);	

	
    int elim = 0;
	if (seconds == 0) {	
        #if TEST
        printf("New matrix \n");
        #endif

		s = s0;
		r = r0;
	
		

		while (1) {
			

			n1 = 0;
			n2 = 0;
			n3 = 0;	
            
		
			for (i = 0; i < s; i++) {
				if (T[i][elim] > TOL) {
					pos[n1] = i;
					n1++;				
				}	
				else if ( T[i][elim] < -TOL) {
					neg[n2]	= i;
					n2++;
				} 
				else {
					zero[n3] = i;
		            n3++;
				}
			}
            #if TEST
            //int* efficiency = malloc(MAX_C*sizeof(int));
            int* n1_vec = calloc(MAX_C,sizeof(int));
            int* n2_vec = calloc(MAX_C,sizeof(int));
            for(j = 0; j < r; j++){
                for (i = 0; i < s; i++) {
				    if (T[i][j] > TOL) {
                        n1_vec[j]++;
				    }	
				    else if ( T[i][j] < -TOL) {
					    n2_vec[j]++;
				    }
			    }
            }
            for(i = 0; i < r; i++){
                printf("%d ", n1_vec[i]*n2_vec[i]-n1_vec[i]-n2_vec[i]);
            }
            printf("\n");
            #endif



			for (i = 0; i < n1; i++) {
                j = pos[i];
                for(k = 0; k < r; k++)				
                   swap[i][k] = T[j][k];
				qswap[i] = q[j];
			}
		    
			for (i = 0; i < n2; i++) {
                j = neg[i];
                for(k = 0; k < r; k++)				
                   swap[n1+i][k] = T[j][k];
				qswap[n1 + i] = q[j];
			}
	
			
			n2 += n1;
			for (i = 0; i < s-n2; i++) {
                j = zero[i];
                for(k = 0; k < r; k++)				
                   swap[n2+i][k] = T[j][k];
				qswap[n2 + i] = q[j];
			}

            for (i = 0; i < s; i++) {
        		for (j = 0; j < r; j++) {
            		printf("%f, ", T[i][j]);
        		}
        		printf(" : %f \n", q[i]);
    		}
            printf("\n");

            
			
            
			//temp = T;
			qtemp = q;

			//T = swap;
			q = qswap;			

			//swap = temp;
			qswap = qtemp;	

            for (i = 0; i < s; i++) {
        		for (j = 0; j < r; j++) {
            		printf("%f, ", swap[i][j]);
        		}
        		printf(" : %f \n", q[i]);
    		}
            printf("\n");	

			for (i = 0; i < n2; i++) {
				q[i] = q[i]/swap[i][elim];
			}
		
			for (i = 0; i < n2; i++) {
				for (j = 1; j < r; j++) {
					swap[i][j] = swap[i][j]/swap[i][elim];
				}
			}

			
			if (r == 1) {
				if ( n1 > 0 && (n2-n1) > 0) {
        			double max = -100000;
					double min = 100000;
					for(i = 0; i < n1; i++) 
						if (q[i] < min)
						    min = q[i];

					for(i=n1; i < n2; i++) 
						if (q[i] > max)
						    max = q[i];

					if (min - max < 0){
						return 0;
					}
				}
				if ((s - n2) > 0) {
					for (i = n2; i < s; i++) 
						if (q[i] < 0) { 
						    return 0;
						}
					return 1;
				}
				else {
					return 1;
				}
			}

			s2 = s - n2 + n1*(n2-n1);
			r--;
			

			for (i = 0; i < n1; i++) {
				for (j = n1; j < n2; j++) {
					for (k = 0; k < r; k++) {
						
						new_T[i*(n2-n1) + j - n1][k] = swap[i][k+1] - swap[j][k+1];
					}
					new_q[i*(n2-n1) + j - n1] = q[i] - q[j];
				}
			}
			
			for (i = 0; i < s-n2; i++) {
				for(j = 0; j < r; j++){
					new_T[n1*(n2-n1) + i][j] = swap[zero[i]][j+1];			
				}
				new_q[n1*(n2-n1) + i] = q[zero[i]];
			}

            for (i = 0; i < s2; i++) {
        		for (j = 0; j < r; j++) {
            		printf("%f, ", new_T[i][j]);
        		}
        		printf(" : %f \n", q[i]);
    		}
            printf("\n");	

			
			//printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
			for (i = 0; i < s2; i++) {
				for (j = 0; j < r; j++) {
					T[i][j] = new_T[i][j];
				}
				q[i] = new_q[i];
			}
			
			s = s2;
		
			//printf("Test 2, %d %d %d\n", s, n1, n2);
		}
	
	}

	/* Tell operating system to call function DONE when an ALARM comes. */
	signal(SIGALRM, done);
	alarm(seconds);

    int MAX_TIMES = 20;
    double timer[MAX_TIMES];
    double timer_sum[MAX_TIMES];
    int timer_counter = 0;
    int timer_full;
    double quotient = 1;

    
	/* Now loop until the alarm comes... */
    #if 0
	proceed = true;
    #if TIMING
	while (fm_count < 500000) {
    #else
	while (proceed) {
    #endif
		s = s0;
		r = r0;
		s2 = 0;

		for (i = 0; i < s; i++) {
			for (j = 0; j < r; j++) {
				T[i][j] = A1[i][j];
			}
			q[i] = c1[i];
		}

		while (1) {
			n1 = 0;
			n2 = 0;
			n3 = 0;	
    
            timer_counter = 0;            
            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif
			/*
			for (i = 0; i < s; i++) {
        		for (j = 0; j < r; j++) {
            		printf("%f, ", T[i][j]);
        		}
        		printf(" : %f \n", q[i]);
    		}
			printf("\n");
			*/
		
			for (i = 0; i < s; i++) {
				if (T[i][elim] > TOL) {
					pos[n1] = i;
					n1++;				
				}	
				else if ( T[i][elim] < -TOL) {
					neg[n2]	= i;
					n2++;
				} 
				else {
					zero[n3] = i;
		            n3++;
				}
			}

			for (i = 0; i < n1; i++) {
				swap[i] = T[pos[i]];
				qswap[i] = q[pos[i]];
			}
		    
			for (i = 0; i < n2; i++) {
				swap[n1 + i] = T[neg[i]];
				qswap[n1 + i] = q[neg[i]];
			}
	
			
			n2 += n1;
            
			for (i = 0; i < s-n2; i++) {
				swap[n2 + i] = T[zero[i]];
				qswap[n2 + i] = q[zero[i]];
			}
			/*
			for (i = 0; i < s; i++) {
				for (j = 0; j < r; j++) {
					printf("&swap[%d][%d] = %zu  ", i,j,&swap[i][j]);
				}
				printf("\n");
			}
			*/	
			/*
			for (i = 0; i < s; i++) {
        		for (j = 0; j < r; j++) {
            		printf("%f, ", swap[i][j]);
        		}
        		printf(" : %f \n", qswap[i]);
    		}
			printf("\n");
			*/
			temp = T;
			qtemp = q;

			T = swap;
			q = qswap;			

			swap = temp;
			qswap = qtemp;
			/*
			
			for (i = 0; i < s; i++) {
				
					printf("&swap[%d] = %zu , &T[%d] = %zu ", i,&swap[i], i,&T[i]);
				
				printf("\n");
			}
			*/

            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif
            /*
			for (i = 0; i < n2; i++) {
				q[i] = q[i]/T[i][r-1];
			}
		    */
			for (i = 0; i < n2; i++) {
                quotient = 1.0/T[i][elim];
                q[i] *= quotient;
				for (j = 1; j < r; j++) {
					T[i][j] *= quotient;
				}
			}

            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif

			
		
			//printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
			if (r == 1) {
				if ( n1 > 0 && (n2-n1) > 0) {
        			double max = -100000;
					double min = 100000;
					for(i = 0; i < n1; i++) 
						if (q[i] < min)
						    min = q[i];

					for(i=n1; i < n2; i++) 
						if (q[i] > max)
						    max = q[i];

					if (min - max < 0){
                        //printf("0");
						break;
					}
				}
				if ((s - n2) > 0) {
					for (i = n2; i < s; i++) 
						if (q[i] < 0) { 
                            //printf("0");
						    break;
						}
                    //printf("1");
					break;
				}
				else {
                    //printf("1");
					break;
				}
			}

			s2 = s - n2 + n1*(n2-n1);
			r--;
			
		    

			/*
		    for (i = 0; i < 1; i++) {
        		for (j = 0; j < 1; j++) {
            		printf("%f, ", T[i][j]);
        		}
        		//printf(" : %f \n", q[i]);
    		}
			//printf("\n");
			*/
			//printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
			/*
			for (i = 0; i < s2; i++) {
				for (j = 0; j < r; j++) {
					printf("&T[%d][%d] = %zu  ", i,j,&T[i][j]);
					printf("&new_T[%d][%d] = %zu  ", i,j,&new_T[i][j]);
					printf("&temp[%d][%d] = %zu  ", i,j,&temp[i][j]);
					printf("&A1[%d][%d] = %zu  ", i,j,&A1[i][j]);
					printf("\n");
				}
			}
			*/
			
			for (i = 0; i < n1; i++) {
				for (j = n1; j < n2; j++) {
					for (k = 0; k < r; k++) {
						//printf("&T[%d][%d] = %zu, &T[%d][%d] = %zu, new_T[%d][%d] = %zu\n", i, k, &T[i][k], j,k,&T[j][k], i*(n2-n1) + j 									- n1, k, &new_T[i*(n2-n1) + j - n1][k])
						/*
						printf("i = %d, j = %d, k = %d\n", i, j, k);
						printf("T[i][k] = %f, T[j][k] = %f\n", T[i][k], T[j][k]);
						printf("i*(n2-n1) + j - n1 = %d\n", i*(n2-n1) + j - n1);
						printf("new_T[i*(n2-n1) + j - n1][k] = %f\n", new_T[i*(n2-n1) + j - n1][k]);
						printf("\n");
						*/
						new_T[i*(n2-n1) + j - n1][k] = T[i][k+1] - T[j][k+1];
						
					}
					new_q[i*(n2-n1) + j - n1] = q[i] - q[j];
				}
			}
            
            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif

			
			for (i = 0; i < s-n2; i++) {
				for(j = 0; j < r; j++){
					new_T[n1*(n2-n1) + i][j] = T[zero[i]][j+1];			
				}
				new_q[n1*(n2-n1) + i] = q[zero[i]];
			}
			
			//printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);
			//temp = T;
			//qtemp = q;

            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif
			
			for (i = 0; i < s2; i++) {
				for (j = 0; j < r; j++) {
					T[i][j] = new_T[i][j];
				}
				q[i] = new_q[i];
			}
			
			//new_T = temp;
			//new_q = qtemp;
			/*
			for (i = 0; i < MAX_R; i++) {
				
					printf("&new_T[%d] = %zu , &T[%d] = %zu ", i,&new_T[i], i,&T[i]);
				
				printf("\n");
			}
			*/
			s = s2;
		
			//printf("Test 2, %d %d %d\n", s, n1, n2);

            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
            #endif

            #if TIMING
                gettimeofday(&tv, NULL);
                timer[timer_counter] = tv.tv_sec + 1e-6 * tv.tv_usec;
                timer_counter++;
                timer_full = timer_counter;

                for(i = 0; i < timer_counter -1 ; i++){
                    timer_sum[i] += timer[i+1]-timer[i];
                }
            #endif
		
		}
		
		fm_count++;
	}
    #endif
    printf("\n");
    for(i = 0; i < timer_full -1 ; i++){
        printf("%f \n",timer_sum[i]);
    }
    printf("\n");
    //printf("%f",T[0][0]);
	return fm_count;
}

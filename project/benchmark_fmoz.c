//benchmark_fmoz.c

#include<stdio.h>
#include<stdlib.h>

#define TOL		(0.000001)

double** get_A_Matrix(int m){
    double** A;
	
	int i;
	A = calloc(m, sizeof(double*));
	for (i = 0; i < m; i++) 
		A[i] = calloc(m, sizeof(double));

	A[0][0] = 5;
	A[0][1] = -2;
	A[1][0] = -15;
	A[1][1] = 6;
	

	return A;
	
}

double* get_c_Matrix(int m){
    double* c;
	c = calloc(m, sizeof(double));
	c[0] = -8;
	c[1] = -2;
		

	return c;
}


char* get_result(){
    //layyhteeeer!
}

void print_result(int a) {
    if (a)
        printf("Has solution\n");
    else
        printf("Has no solutions :C \n");
}

int make_solution(double* q, int s, int n1, int n2){
    int i;

    if ( n1 > 0 && (n2-n1) > 0) {
        int max = -100000;
        int min = 100000;
        for(i = 0; i < n1; i++) 
            if (q[i] > max)
                max = q[i];

        for(i=n1; i < n2; i++) 
            if (q[i] < min)
                min = q[i];

        if (min - max < 0)
            return 0;
        else
            return 1;
    }
    else if ((s - n2) > 0) {
        for (i = n2; i < s; i++) 
            if (q[i] < 0) { 
                return 0;
            }
        return 1;
    }
    else {
        return 1;
    }
}


int fourier_motzkin_elimination(double** A, double* c, int m){
	int r = m;
    int s = m;
	double** T = A;
   
	double* q = c;
	int a = 1;
	while (a > 0) {
		 
		int n1 = 0;
		int n2 = 0;
		double** pos = malloc(s*sizeof(double*));
		double** neg = malloc(s*sizeof(double*));	
		double** zero = malloc(s*sizeof(double*));
		int i;
		for (i = 0; i < s; i++) {
			pos[i] = malloc(r*sizeof(double));
			neg[i] = malloc(r*sizeof(double));
			zero[i] = malloc(r*sizeof(double));
		}
		double* pos_q = malloc(s*sizeof(double));
		double* neg_q = malloc(s*sizeof(double));
		double* zero_q = malloc(s*sizeof(double));
				

		for (i = 0; i < s; i++) {
			if (T[i][(r-1)] > TOL) {
				pos[n1] = T[i];
				pos_q[n1] = q[i];
				n1++;				
			}	
			else if ( T[i][r-1] < -TOL) {
				neg[n2]	= T[i];
				neg_q[n2] = q[i];
				n2++;
			} 
			else {
				zero[i] = T[i];
				zero_q[i] = q[i];
			}
		}
        
			
		
		for (i = 0; i < n2; i++) {
			pos[n1 + i] = neg[i];
			pos_q[n1+i] = neg_q[i];
		}
		n2 += n1;
		for (i = 0; i < s; i++) {
			pos[n2 + i] = zero[i];
			pos_q[n2 + i] = zero_q[i];
		}

        


		T = pos;
		q = pos_q;

		for (i = 0; i < n2; i++) {
			q[i] /= T[i][r-1];
		}
		int j;
		for (i = 0; i < r; i++) {
			for (j = 0; j < s; j++) {
				T[i][j] /= T[i][r-1];
			}
		}
        
		
		if (r == 1) {
			return make_solution(q, s, n1, n2);
		}

		int s2 = s - n2 + n1*(n2-n1);
		r--;
		
		
		double** new_T = malloc((s2 + s-n2)*sizeof(double*));
		double* new_q = malloc((s2 + s - n2)*sizeof(double));
		for (i = 0; i < (s2 + s - n2); i++) {
			new_T[i] = malloc(r*sizeof(double));
		}
		
		printf("Test, r=%d s2=%d s1=%d n1=%d n2=%d\n", r, s2, s, n1, n2);	
		int k;
		for (i = 0; i < n1; i++) {
			for (j = n1; j < n2; j++) {
				for (k = 0; k < r; k++) {
					new_T[i*(n2-n1) + j - n1][k] = T[i][k] - T[j][k];
				}
				new_q[i*(n2-n1) + j - n1] = q[i] - q[j];
			}
		}
		
		for (i = 0; i < s-n2; i++) {
			new_T[s2 + i] = zero[i];
			new_q[s2 + i] = zero_q[i];
		}
		T = new_T;
		q = new_q;
		s = s2;
		
		printf("Test 2, %d %d %d\n", s, n1, n2);
		

	}


}



/*
int main()
{
	int m = 2;
	
    double** A = get_A_Matrix(m);
    double* c = get_c_Matrix(m);

	int i;
	int j;
	for (i = 0; i < m; i++) {
		for (j = 0; j < m; j++) {
			printf("%f ", A[i][j]);
		}
		printf(": %f \n",c[i]);
	}

	int has = fourier_motzkin_elimination(A, c, m);


    //char* facit get_result()
    

}
*/

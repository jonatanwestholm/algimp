#parser for the project in python

import os
import sys
curr_dir = os.getcwd()


def parse_file(file_number):
    f = open(curr_dir + "/input/{0:d}/A".format(file_number),'r')
    g = open(curr_dir + "/input/{0:d}/c".format(file_number),'r')
    h = open(curr_dir + "/input/{0:d}/result".format(file_number),'r')
    

    A = []
    ln = f.readline()
    ln = f.readline()
    while(len(ln) > 0):
        A.append([int(x) for x in ln.split()])
        ln = f.readline()

    c = []
    ln = g.readline()
    ln = g.readline()
    while(len(ln) > 0):
        c.append(int(ln))
        ln = g.readline()

    return A,c,h.read()
    

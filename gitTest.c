#include <stdio.h>


struct list_t {
  int first;
  int second;
};

struct list_c {
  char* this;
  char* that;
};


int main(int argc, char** argv)
{

  int a = 1;
  int b = 1;
  int* c = &b;

  f(c);

  struct list_t s1;
  s1.first = 10;
  s1.second = 20;

  struct list_c s2;
  s2.this = "Hej";
  s2.that = "420blazeit";
  
  printf("Print from numbers: %d, %d\n", a, b);
  printf("Print from first struct: %d, %d\n", s1.first, s1.second);
  printf("Print from second struct: %s, %s\n", s2.this, s2.that);
  
  return 0;
}



int f(int* c)
{
  *c += 1;
  return 0;
}

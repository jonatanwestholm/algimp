//exc121 must be compiled with the -lm flag!

#include<math.h>
#include<stdio.h>

int main(void)
{

    double x[4] = {0.0,0.1,0.2,0.3};
    int i;
    double root, power,xi;
    for(i = 0; i < 4; i++){
        xi = x[i];
        root = sqrt(xi);
        power = exp(xi);
        printf("sqrt(x): %lf, exp(x): %lf \n",root,power);    
    }

}

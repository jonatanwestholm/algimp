#exc123.py 

import time

s = 1
t = time.clock()
i = 2
while s < 18:
    s += 1.0/i
    i += 1

print time.clock()-t 

#note the awesome time difference between C and python here

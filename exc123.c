//exc123 must be compiled with the -lm flag!

#include<stdio.h>
#include<time.h>

int main()
{
    double sum = 1;
    int i = 2;
    clock_t start_t, end_t;
    start_t = clock();
    while(sum < 18){
        sum += 1.0/i;
        i += 1;
    }
    end_t = clock();
    printf("runtime: %lf \n",(double) (end_t-start_t)/CLOCKS_PER_SEC);

}
